using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Jump : MonoBehaviour
{
    private Rigidbody rigidBody;

    private float movementSpeed = 1f;

    private Vector3 targetVector;

    private bool jumpComplete = false;

    void Start()
    {
        this.rigidBody = GetComponent<Rigidbody>();

        this.targetVector = Vector3.up * 0.3f;
    }

    void Update()
    {
        if(Vector3.Distance(Camera.main.transform.position, transform.position) < 0.3f && !jumpComplete)
        {
            this.rigidBody.AddForce(this.targetVector * this.movementSpeed, ForceMode.Impulse);

            // I prefer the drone to jump every time it is within .3m of the camera :)
            //this.jumpComplete = true;
        }
    }
}
