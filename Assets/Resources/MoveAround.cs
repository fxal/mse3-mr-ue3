using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;

public class MoveAround : MonoBehaviour, IMixedRealityTouchHandler
{

    [SerializeField]
    private Animator animator;

    private bool wasTouched = false;

    public void OnTouchStarted(HandTrackingInputEventData eventData)
    {
        string ptrName = eventData.ToString();
        Debug.Log($"Touch started from {ptrName}");

        if(wasTouched)
        {
            this.animator.enabled = false;
            this.animator.ResetTrigger("Start");
            wasTouched = false;

            Debug.Log("Stopped Animation");
        } 
        else
        {
            this.animator.enabled = true;
            this.animator.SetTrigger("Start");
            wasTouched = true;

            Debug.Log("Started Animation");
        }
    }

    public void OnTouchCompleted(HandTrackingInputEventData eventData)
    {
        //string ptrName = eventData.ToString();
        //Debug.Log($"Touch completed from {ptrName}");

    }
    public void OnTouchUpdated(HandTrackingInputEventData eventData)
    {
        //string ptrName = eventData.ToString();
        //Debug.Log($"Touch updated from {ptrName}");
        
    }

    // Start is called before the first frame update
    void Start()
    {
        this.animator.enabled = false;
    }

}
