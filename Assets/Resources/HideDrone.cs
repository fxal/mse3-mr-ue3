using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideDrone : MonoBehaviour
{

    void Update()
    {
        var render = this.gameObject.GetComponentInChildren<MeshRenderer>();
        render.enabled = Vector3.Distance(Camera.main.transform.position, transform.position) < 1f;
    }
}
