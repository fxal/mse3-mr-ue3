MSE WS2021 MR UE3
Author: Felix Almer se20m001
se20m001@technikum-wien.at

Target: Hololens 1

Done:
* Playspace erscheint, sobald ein passendes ImageTarget gefunden wurde (zu Testzwecken in Unity Game Mode das Playspace GameBody die Hierarchie hinaufschieben) => Vuforia
* Drohne wird nicht mehr gerendert, wenn die Kamera > 1m entfernt ist, die Grundfläche bleibt bestehen => HodeDrone.cs
* Drohne springt .3m wenn sich die Kamera auf 30cm nähert => Jump.cs
* Drohne beginnt eine Animation, bei der sie sich vor und zurück bewegt, sobald sie per TouchInteraction getriggert wird. Die Animation hört auf, sobald sie erneut von TouchInteraction betroffen ist => MoveAround.cs

Probleme:
* Schatten konnten leider nicht realisiert werden
* Im Unity Game Mode funktionieren die oben beschriebenen Verhalten problemlos, bei Deploy auf die Hololens ist die Drohne aus unerfindlichen Gründen nicht sichtbar (evtl weil sie ein RigidBody hat für den Sprung?)
